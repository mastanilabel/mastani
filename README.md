Mastani is the vision of Kudrat Makkar, the founder and creative director. Through Mastani, she wants to take the customers to a journey into the world of the hands that made it and the culture that shaped it. By using handmade traditional techniques which are inherited by the artisans from their ancestors. Her aim is to preserve and revitalise her cultural heritage through Mastani.

Website : https://mastanilabel.com/
